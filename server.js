const express = require('express');
const server = express();
const bodyParser = require('body-parser');
const path = require('path');

server.use(bodyParser.json({ limit: '10mb' })); // support json encoded bodies
server.use(bodyParser.urlencoded({ limit: '10mb', extended: true })); // support encoded bodies
server.all('*', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Accept');
    next();
});

let router = express.Router();
server.use('/',router);

const ROUTE_GVISION_OCR = require('./src/rest/gvision_ocr');
const route_gvision_ocr = new ROUTE_GVISION_OCR(router);

router.use((result, req, res, next) => {});

const PORT = 3000;

server.listen(PORT, function () {
    console.log("Google Vision OCR server started on port " + PORT);
});
