const GoogleVisionOCR = require('../lib/google/ocr');

module.exports = class GVISION_OCR {
    constructor(router) {
        router.post('/ocr/gvision', (req, res, next) => {
            let imageObject = req.body;

            if (!imageObject.filePath) {
                if (!imageObject.base64String) {
                    res.status(400).send({
                        'Error': 'Missing image file path or its base 64 string'
                    });
                    next();
                }
            }
            
            GoogleVisionOCR.ocr(imageObject, (err, result) => {
                if (err) {
                    res.status(400).send({
                        'Error': err
                    });
                    next();
                }

                res.status(200).send(result);
            });
        });
    }
}
