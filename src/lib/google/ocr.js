const COMMON = require('../tools/common');
const BASE64WRITER = require('../tools/base64Writer');

const TEMP_BASE64 = '/data/temp/base64_converted/';
const TEMP_RESIZE = '/data/temp/resized/';

const path = require('path');
const fs = require('fs-extra');
const shell = require('shelljs');
const IM = require('imagemagick');
const GoogleVision = require('@google-cloud/vision');

const CONFIG = require('../../../config/ocr-config.json');

module.exports = {
    ocr : process
};

function process (imageObject, callback) {
    if (!typeof imageObject === 'object') {
        let err = 'Parsed variable must be an OBJECT'
        return callback(err);
    };

    if (!imageObject.filePath) {
        if (!imageObject.base64String) {
            let err = 'This image must be a file or included with base64 string';
            return callback(err);
        }
    } else {
        if (!COMMON.checkPathExists(imageObject.filePath)) {
            let err = 'File from path is missing';
            return callback(err);
        }
    }

    if (imageObject.base64String) {
        if (!COMMON.checkPathExists(TEMP_BASE64)) {
            shell.mkdir('-p', TEMP_BASE64);
        }

        let fileExtension = BASE64WRITER.getExtension(BASE64WRITER.getBase64MimeType(imageObject.base64String)),
            fileName = imageObject.name || COMMON.generateTempName();
            tempFilePath = TEMP_BASE64 + fileName + '.' + fileExtension;
        
        BASE64WRITER.write(imageObject.base64String, tempFilePath, (err,resut) => {
            if (err) return callback(err);

            imageObject.filePath = tempFilePath;

            detectText(imageObject, (err, resultText, apiResponse) => {
                if (err) return callback(err);
                
                let detectedResult = {
                    response : apiResponse,
                    text : resultText
                };
                shell.rm('-f', imageObject.filePath);
                return callback(null, detectedResult);
            });
        });
    } else {
        detectText(imageObject, (err, resultText, apiResponse) => {
            if (err) return callback(err);
            
            let detectedResult = {
                response : apiResponse,
                text : resultText
            };

            return callback(null, detectedResult);
        });
    }
}

function detectText(imageObject, callback) {
    let visionOCR = GoogleVision({
        projectId: 'total-acumen-171906',
        keyFilename: path.join(__dirname, '../../../config/Project-X-6177ce5c8d32.json')
    });

    let fileExtension = COMMON.getFileExtension(imageObject.filePath),
        fileSize = parseFloat(fs.statSync(imageObject.filePath).size) / 1000000.0;

    if (CONFIG.supportedExtensions.indexOf(fileExtension) <= -1 || parseFloat(CONFIG.maxFileSize_mb) < fileSize) {
        resizeImage(imageObject.filePath, (err, resultConverted) => {
            if (err) return callback(err);
            imageObject.filePath = resultConverted;

            visionOCR.textDetection({ source: { filename: imageObject.filePath } })
            .then((results) => {
                let detections = results[0].textAnnotations;
                shell.rm('-f', imageObject.filePath);
                return callback(null, detections, results);
            })
            .catch((err) => {
                return callback(err);
            });
        });
    } else {
        visionOCR.textDetection({ source: { filename: imageObject.filePath } })
        .then((results) => {
            let detections = results[0].textAnnotations;
    
            return callback(null, detections, results);
        })
        .catch((err) => {
            return callback(err);
        });
    }
}

function resizeImage(filePath, callback) {
    if (!COMMON.checkPathExists(TEMP_RESIZE)) {
        shell.mkdir('-p', TEMP_RESIZE);
    }

    let maxFileSize = parseInt(CONFIG.maxFileSize_mb) * 1024,
        options = 'jpeg:extent='+maxFileSize+'kb';

    let outputPath = TEMP_RESIZE + COMMON.getFileNameOnly(filePath) + '.jpg'; 

    let args = [
        filePath,
        '-define',
        options, 
        outputPath
    ];

    IM.convert(args, (err, stdout) => {
        if (err) return callback(err);
        return callback(null, outputPath);
    });
}
