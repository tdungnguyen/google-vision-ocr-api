const COMMON = require('./common');
const fs = require('fs');

function writeToFile(base64Str, outputPath, callback) {
    if (COMMON.checkPathExists(COMMON.getFolderPath(outputPath)) && !COMMON.checkPathExists(outputPath)) {
        var buffer = new Buffer (base64Str.split(',')[1], "base64");
        if (getExtension(outputPath) !== getExtension(base64MimeType(base64Str))) {
            outputPath = COMMON.getFolderPath(outputPath) + COMMON.getFileNameWithExtension(COMMON.getFilePathWithoutExtension(outputPath) + "." + getExtension(base64MimeType(base64Str)));
        }

        fs.writeFile(outputPath, buffer, (err)=> {
            if (err) {
                return callback(err);
            }

            if (COMMON.checkPathExists(outputPath)) {
                return callback(null, outputPath);
            }
        });
    } else {
        return callback("Output path "+COMMON.getFolderPath(outputPath)+" is missing");
    }
}

function getExtension(mimeStr){
    return mimeStr.split('/')[1];
}

function base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
        return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
        result = mime[1];
    }
    return result;
}

module.exports = {
    write : writeToFile,
    getExtension : getExtension,
    getBase64MimeType : base64MimeType
};
