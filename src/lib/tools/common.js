const fs = require('fs');
const util = require("util");
var mime = require("mime");
const readChunk = require('read-chunk');
const fileType = require('file-type');
const regex_name = /(?:\.([^.]+))?$/;
const shell = require('shelljs');

function convertToBase64 (file){
    var data = fs.readFileSync(file).toString("base64");
    return util.format("data:%s;base64,%s", mime.lookup(file), data);
}

function getFileNameOnly(file_path) {
    return getFileNameWithExtension(getFilePathWithoutExtension(file_path));
}

function getFileExtension(file_path){
    if (checkPathExists(file_path)) {
        var buffer = readChunk.sync(file_path, 0, 4100);
        var file_extension;
        
        if (fileType(buffer)) {
            file_extension = fileType(buffer).ext; 
            return file_extension;
        }
    } else {
        return (regex_name.exec(file_path)[1]).toString();
    }
    
    return file_extension;
}

function checkPathExists(folderPath){
    if (fs.existsSync(folderPath)){
        return true;
    } else {
        return false;
    }
}

function getFilePathWithoutExtension(file){  
    return file.substring(0, file.lastIndexOf('.'));
}

function getFileNameWithExtension(file_path){ //from full file path
    return file_path.replace(/^.*[\\\/]/, '');
}

function getFolderPath(file_path) {
    return file_path.substring(0, file_path.lastIndexOf("/")+1);
}

function generateTempName(){
    var name = '';
    for (var i = 0; i < 8; i++) {
        name += Math.floor(Math.random() * 16).toString(16);
    }
    name = new Date().getTime() +'-'+name;
    return name;
}

module.exports = {
    convertToBase64 : convertToBase64,
    getFileExtension : getFileExtension,
    getFileNameOnly : getFileNameOnly,
    getFileNameWithExtension : getFileNameWithExtension,
    getFilePathWithoutExtension : getFilePathWithoutExtension,
    checkPathExists : checkPathExists,
    getFolderPath : getFolderPath,
    generateTempName : generateTempName
}
